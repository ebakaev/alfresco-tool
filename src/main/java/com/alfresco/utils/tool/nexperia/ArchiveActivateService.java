package com.alfresco.utils.tool.nexperia;

import org.springframework.web.client.RestTemplate;

public class ArchiveActivateService {

    private final RestTemplate restTemplate;
    private final String alfrescoUrl;

    public ArchiveActivateService(RestTemplate restTemplate, String alfrescoUrl) {
        this.restTemplate = restTemplate;
        this.alfrescoUrl = alfrescoUrl;
    }

    public void archiveDrawingByNodeRefString(String drawingNodeRef) {
        /* Request new ticket */
        String response = restTemplate.getForEntity(String.format("%s/alfresco/s/drawing/archive?nodeRef=%s", alfrescoUrl, drawingNodeRef), String.class).getBody();
        System.out.println(response);
    }

    public void activateDrawingByNodeRefString(String drawingNodeRef) {
        /* Request new ticket */
        String response = restTemplate.getForEntity(String.format("%s/alfresco/s/drawing/activate?nodeRef=%s", alfrescoUrl, drawingNodeRef), String.class).getBody();
        System.out.println(response);
    }

}
