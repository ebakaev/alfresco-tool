package com.alfresco.utils.tool.service;

import org.springframework.web.client.RestTemplate;

import java.util.Objects;

/*
*  Gets Alfresco ticket from any alfresco instance if necessary
**/
public class AlfrescoTicketService {

    private final RestTemplate restTemplate;
    private final String alfrescoUrl;
    private String alfTicket;

    public String getTicket() {
        return alfTicket != null ? alfTicket : getNewTicket();
    }

    private String getNewTicket() {
        /* Request new ticket */
//        String response = restTemplate.getForEntity(String.format("%s/alfresco/s/api/groups/ALFRESCO_ADMINISTRATORS", alfrescoUrl), String.class).getBody();
//        System.out.println(response);
        String response = restTemplate.getForEntity(String.format("%s/alfresco/s/api/login?u=%s&pw=%s", alfrescoUrl, "admin", "admin"), String.class).getBody();
        this.alfTicket = parseTicket(Objects.requireNonNull(response));
        return alfTicket;
    }

    private static String parseTicket(String ticketXML) {
        return ticketXML.substring(ticketXML.indexOf("<ticket>") + "<ticket>".length(), ticketXML.indexOf("</ticket>"));
    }

    public AlfrescoTicketService(RestTemplate restTemplate, String alfrescoUrl) {
        this.restTemplate = restTemplate;
        this.alfrescoUrl = alfrescoUrl;
    }

}
