package com.alfresco.utils.tool;

import com.alfresco.utils.tool.nexperia.ArchiveActivateService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@SpringBootApplication
public class ToolApplication {

    public static void main(String[] args) {
        //SpringApplication.run(ToolApplication.class, args).close();
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ToolApplication.class, args);
        String alfrescoUrl = applicationContext.getEnvironment().getProperty("app.alfresco.url");
        String alfrescoUsername = applicationContext.getEnvironment().getProperty("app.alfresco.username");
        String alfrescoUserPassword = applicationContext.getEnvironment().getProperty("app.alfresco.password");
        String archiveActivateFileLocation = applicationContext.getEnvironment().getProperty("app.alfresco.archive.activate.file.location");

        RestTemplate restTemplate = new RestTemplateBuilder().basicAuthentication(alfrescoUsername, alfrescoUserPassword).build();

        List<String> list = new ArrayList<>();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(archiveActivateFileLocation))) {
            //br returns as stream and convert it into a List
            list = br.lines().collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        processArchiveActivateActions(restTemplate, alfrescoUrl, list);

        applicationContext.close();
    }

    /*
    * Runs remove Archive or Activate WS of provided Alfresco by URL and using Basic auth.
    * */
    public static void processArchiveActivateActions(RestTemplate restTemplate, String alfrescoUrl, List<String> list) {
        AtomicInteger count = new AtomicInteger(1);
        final int totalCount = list.size();
        ArchiveActivateService archiveActivateService = new ArchiveActivateService(restTemplate, alfrescoUrl);
        /* Archive a list of Drawings by NodeRefs */
        list.forEach(drawing -> {
            archiveActivateService.archiveDrawingByNodeRefString(drawing);
            System.out.println("[" + count.getAndIncrement() + " of " + totalCount + "] processed...");
        });
        /* Activate a list of Drawings by NodeRefs */
//        list.forEach(drawing -> {
//            archiveActivateService.activateDrawingByNodeRefString(drawing);
//            System.out.println("[" + count.getAndIncrement() + " of " + totalCount + "] processed...");
//        });

    }
}
