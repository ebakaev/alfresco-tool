# Alfresco tool application

### Archive/Activate a list of Drawings by their NodeRefs in Drawing 2.0 module.

1. You need to prepare a file with a list of NodeRefs to archive or activate.
2. Put the full path to the file in code (hardcoded).
3. Update credentials in the code (hardcoded).
4. Update application.properties to point on Alfresco location.

